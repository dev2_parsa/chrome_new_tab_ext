# Semester New Tab Chrome Extenion

Customized Chrome 'New Tab' Extension, which can be updated to reflect current links each semester


Extensions are small browser add-ons that add new and useful features to Chrome.

## Links

- [View this extension in the Chrome Web Store](
https://chrome.google.com/webstore/detail/custom-web-launcher/bjcnlknhmbnejpgmpblndpddpckjhomm?hl=en)

- [Check out the code on BitBucket](https://bitbucket.org/sigcse2018/chrome_home_nwmsu)
- [Review the lessons on Canvas](https://nwmissouri.instructure.com/courses/16155)

## SIGCSE 2018 Workshop

Come to [SIGCSE 2018](https://sigcse2018.sigcse.org/)  to learn how to customize the extension.

[302: Chrome Home: Six Fun Activities Introducing Basic Web Programming Techniques](https://sigcse2018.sigcse.org/attendees/workshops.html)

Denise M. Case and Douglas Hawley

Northwest Missouri State University


Friday, February 23, 7:00 pm - 10:00 pm
Room: 302

This workshop will provide participants with several small, fun classroom activities culminating in a useful web-based application that allows individuals to fully customize the page resulting from opening a new tab in Google Chrome. Attendees will participate in and receive Canvas lessons introducing popular web-based techniques including HTML, JSON, Cascading Style Sheets, JavaScript and Google Chrome extension creation and distribution. The workshop proceeds in six short lessons in which we will:
1.	download and install the free software required, 
2.	introduce basic concepts in HTML,
3.	create and link cascading style sheets (CSS),
4.	construct a JavaScript file implementing some basic logic constructs into our web page,
5.	create a JSON manifest file to enable our app to serve as a Chrome extension, and 
6.	make the extension available in the chrome web store (if desired, small cost)
 
Participants will create a “New Tab” extension for their school that can be customized and shared.

## References & Links

- [Dillinger Markdown Editor](http://dillinger.io/)
- [Bing Image URL](http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1)
- [Stack Overflow Question](https://stackoverflow.com/questions/10639914/is-there-a-way-to-get-bings-photo-of-the-day/45472526#45472526)
- [Clock](
https://www.ricocheting.com/code/javascript/html-generator/date-time-clock)

## Install Tools

Install [Chocolately](https://chocolatey.org/install), a package manager for Windows. 

Chocolately makes it easy to install our other tools: 
- Git for Windows (distributed version control system)
- TortoiseGit (integrates Git with Windows File Explorer)
- Visual Studio Code (code editor)

Open a PowerShell window as an administrator. Right-click on  Windows Start icon and click "Open PowerShell (Admin)".

Run the following commands:

```PowerShell
choco install git
choco install tortoisegit
choco install visualstudiocode
```

## Start Editing

- Sign up for a BitBucket account.
- Go to https://bitbucket.org/professorcase/chrome_new_tab_ext. 
- [Fork the repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) into your BitBucket account in the cloud. 
- [Clone your repo](https://tortoisegit.org/docs/tortoisegit/tgit-dug-clone.html) down to your local machine.
- Right-click in your new "chrome_new_tab_ext" folder and select "Open With Code"


