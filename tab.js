const tday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
const tmonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const semester_start_week_number = 2

function GetClock() {
  const d = new Date()
  const nday = d.getDay()
  const nmonth = d.getMonth()
  const ndate = d.getDate()
  const nyear = d.getFullYear()
  let nhour = d.getHours()
  let nmin = d.getMinutes()
  let ap
  if (nhour === 0) { ap = ' AM'; nhour = 12 } else if (nhour < 12) { ap = ' AM' } else if (nhour === 12) { ap = ' PM' } else if (nhour > 12) { ap = ' PM'; nhour -= 12 }

  if (nmin <= 9) nmin = '0' + nmin

  document.getElementById('clockbox').innerHTML = '' + tday[nday] + ' ' + tmonth[nmonth] + ' ' + ndate + '  ' + nhour + ':' + nmin + ap + ''
}

function GetGreeting() {
  const d = new Date()
  const nhour = d.getHours()
  var t = 'Good evening'
  if (nhour < 12) { t = 'Good morning' } else if (nhour < 17) { t = 'Good afternoon' }

  document.getElementById('greeting').innerHTML = t + ', Dr. Case!'
}

function GetFocus() {
  const d = new Date()
  const nday = d.getDay()
  const nhour = d.getHours()
  var t = ''
  if ((nday === 'Mon' || nday === 'Wed' || nday === 'Fri') && nhour === 12) { t = '44-563 Web Apps 04' }
  else if ((nday === 'Mon' || nday === 'Wed' || nday === 'Fri') && nhour === 10) { t = '44-663 C# 01' }
  else t = 'Grants'

  document.getElementById('focus').innerHTML = t
}

Date.prototype.getWeekNumber = function () {
  const d = new Date();
  d.setHours(0, 0, 0, 0);
  d.setDate(d.getDate() + 4 - (d.getDay() || 7));
  return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};


function GetWeek() {
  const d = new Date(+this)
  let nweek = d.getWeekNumber() - semester_start_week_number + 1
  document.getElementById('week').innerHTML = 'Week ' + nweek + ' of ' + 16
}

window.onload = function () {

  UpdateBackgroundImage('https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1')

  GetClock()
  setInterval(GetClock, 1000)

  GetGreeting()
  setInterval(GetGreeting, 1000)

  GetFocus()
  setInterval(GetFocus, 1000)

  GetWeek()
}


/**
 * Update background image with Bing Image of the Day
 * @param {String} url 
 * @return {Object} response
 */
function UpdateBackgroundImage(url) {
  let xmlhttp = new window.XMLHttpRequest()

  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) { // when done & successful
      console.log('Response received: ' + this.responseText)
      var imageURL = ExtractImageURL(JSON.parse(this.responseText));
      console.log('Image url: ' + imageURL)
      SetBackgroundImage(imageURL)
    }
  };
  xmlhttp.open("GET", url, true)  // define the request as a get, to this url, asynchronous
  xmlhttp.send()                 // actually send request to server
}

/**
 * Extract first image URL from Bing response JSON.
 * @param {Object} response
 * @return {String} imageURL 
 */
function ExtractImageURL(response) {
  var suffix = response.images[0].url
  return 'http://bing.com/' + suffix 
}

/**
 * Set background image to imageURL
 * @param {String} imageURL 
 */
function SetBackgroundImage(imageURL) {
  document.getElementById("background").style.backgroundImage = 'url("' + imageURL + '"'
}